const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({ origin: true }));

var serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://exercice2-ab407.firebaseio.com"
});
const db = admin.firestore();

// create bot
app.post('/api/createBot', (req, res) => {
    (async () => {
        try {
            console.log("req", req)
            await db.collection('Bots').doc('/' + req.body.id + '/').create({ bot: req.body.bot });
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();

    /*
        Postman : 
            {
                "id": "KdbKcU2ptfYF2xKb5aO",
                "bot": {
                    "name": "Eram",
                    "actions": {
                        "PAYLOAD_GET_STARTED": {
                            "title": "Démarrage",
                            "type": "message"
                        },
                        "PAYLOAD_CLIENT": {
                            "title": "client",
                            "type": "message"
                        }
                    },
                    "users": ["foyhrfrdste", "zsbcfrehr"]
                }
            }
    
    */
});

// update
app.put('/api/updateUsers/:bot_id', (req, res) => {
    (async () => {
        try {
            const document = db.collection('Bots').doc(req.params.bot_id);
            await document.update({
                "bot.users": admin.firestore.FieldValue.arrayUnion(req.body.newUserId)
            });
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();

    /* requst url : http://localhost:5000/exercice2-ab407/us-central1/app/api/updateUsers/KdbKcU2ptfYF2xKb5aO  
    {
        "newUserId" : "mouldi"
    }
    */
});

// delete
app.delete('/api/delete/:bot_id', (req, res) => {
    (async () => {
        try {
            const document = db.collection('Bots').doc(req.params.bot_id);
            await document.delete();
            return res.status(200).send();
        } catch (error) {
            console.log(error);
            return res.status(500).send(error);
        }
    })();

    /* requst url : http://localhost:5000/exercice2-ab407/us-central1/app/api/delete/KdbKcU2ptfYF2xKb5aO */
});

exports.app = functions.https.onRequest(app);